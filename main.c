#define _XOPEN_SOURCE 600
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

// pts descriptors
int fdm[2] = { 0, 0 };

// pts link names
char * slavename[2] = { NULL, NULL };

void term_handler(int i)
{
    (void) i;

    printf("Closing pts\n");
    if (NULL != slavename[1]) {
        remove(slavename[1]);
    }
    if (0 != fdm[1]) {
        close(fdm[1]);
    }
    if (NULL != slavename[0]) {
        remove(slavename[0]);
    }
    if (0 != fdm[0]) {
        close(fdm[0]);
    }
    exit(EXIT_SUCCESS);
}

void printf_usage() {
    printf("Usage\n\tpts2pts pts1_name pts2_name\n");
}

int directory_writable(char * path) {
    int result = 0;
    char * lastslash = NULL;
    int dirlen = 0;
    char * dir = NULL;

    lastslash = strrchr(path, '/');
    if (NULL != lastslash) {
        // Get string before last slash
        dirlen = lastslash - path;
        dir = malloc(dirlen + 1);
        strncpy(dir, path, dirlen);
        dir[dirlen] = '\0';
    } else {
        dir = ".\0";
    }

    // Check if directory is writable
    result = (0 == access(dir, W_OK | X_OK));

    // Cleanup
    if (NULL != lastslash)
        free(dir);

    return result;
}

int file_exists(char * path) {
    return (0 == access(path, F_OK));
}

int main(int argc, char * argv[])
{
    // Needed to catch signals
    struct sigaction sa;
    sigset_t newset;

    // Slave device names
    char * pts[2];

    // Variables for poll
    int ohup[2];
    int hup[2];
    struct pollfd fdpoll[2];
    int ret;

    // Copy buffer
    size_t bufsize = 1024;
    char * buf;
    size_t readbytes = 0;
    size_t pts1bytes = 0;
    size_t pts12bytes = 0;
    size_t pts2bytes = 0;
    size_t pts21bytes = 0;

    // Block SIGHUPargv1path
    sigemptyset(&newset);
    sigaddset(&newset, SIGHUP);
    sigprocmask(SIG_BLOCK, &newset, 0);

    // Catch SIGINT and SIGTERM
    sa.sa_handler = term_handler;
    sigaction(SIGINT, &sa, 0);
    sigaction(SIGTERM, &sa, 0);

    if (3 != argc) {
        printf_usage();
        return EXIT_FAILURE;
    }

    if (!directory_writable(argv[1])) {
        fprintf(stderr, "Directory of %s is not writable or doesn't exist.\n", argv[1]);
        fflush(stderr);
        return EXIT_FAILURE;
    }

    if (!directory_writable(argv[2])) {
        fprintf(stderr, "Directory of %s is not writable or doesn't exist.\n", argv[2]);
        fflush(stderr);
        return EXIT_FAILURE;
    }

    if (file_exists(argv[1])) {
        fprintf(stderr, "Can't create %s file allready exists.\n", argv[1]);
        fflush(stderr);
        return EXIT_FAILURE;
    }

    if (file_exists(argv[2])) {
        fprintf(stderr, "Can't create %s file allready exists.\n", argv[2]);
        fflush(stderr);
        return EXIT_FAILURE;
    }

    // Create pts1
    fprintf(stderr, "Creating pts1\n"); fflush(stderr);
    fdm[0] = posix_openpt(O_RDWR | O_NOCTTY);
    grantpt(fdm[0]);
    unlockpt(fdm[0]);
    pts[0] = strdup(ptsname(fdm[0]));

    // Open-close for pollhup on startup
    close(open(pts[0], O_RDWR | O_NOCTTY));

    // Create pts2
    fprintf(stderr, "Creating pts2\n"); fflush(stderr);
    fdm[1] = posix_openpt(O_RDWR | O_NOCTTY);
    grantpt(fdm[1]);
    unlockpt(fdm[1]);
    pts[1] = strdup(ptsname(fdm[1]));

    // Open-close for pollhup on startup
    close(open(pts[1], O_RDWR | O_NOCTTY));

    // Now everything looks OK, so make links to pts devices
    symlink(pts[0], argv[1]);
    symlink(pts[1], argv[2]);
    slavename[0] = argv[1];
    slavename[1] = argv[2];

    fdpoll[0].fd = fdm[0];
    fdpoll[0].events = POLLIN | POLLHUP;

    fdpoll[1].fd = fdm[1];
    fdpoll[1].events = POLLIN | POLLHUP;

    fprintf(stderr, "Starting...\n"); fflush(stderr);
    buf = malloc(bufsize);
    while(1) {
        ret = poll(fdpoll, 2, -1);
        if (ret > 0) {
            ohup[0] = hup[0];
            ohup[1] = hup[1];
            hup[0] = (fdpoll[0].revents & POLLHUP);
            hup[1] = (fdpoll[1].revents & POLLHUP);

            if (ohup[0] != hup[0]) {
                if (0 == hup[0]) {
                    fprintf(stderr, "pts1 opened\n"); fflush(stderr);
                } else {
                    fprintf(stderr, "pts1 closed\n"); fflush(stderr);
                }
            }
            if (ohup[1] != hup[1]) {
                if (0 == hup[1]) {
                    fprintf(stderr, "pts2 opened\n"); fflush(stderr);
                } else {
                    fprintf(stderr, "pts2 closed\n"); fflush(stderr);
                }
            }

            if (0 == ((fdpoll[0].revents & POLLIN) | (fdpoll[1].revents & POLLIN))) {
                usleep(100 * 1000);
            } else {
                if (0 != (fdpoll[0].revents & POLLIN)) {
                    readbytes = read(fdm[0], buf, sizeof(buf));
                    pts1bytes += readbytes;
                    if (0 == hup[1]) {
                        write(fdm[1], buf, readbytes);
                        pts21bytes += readbytes;
                    }
                }
                if (0 != (fdpoll[1].revents & POLLIN)) {
                    readbytes = read(fdm[1], buf, sizeof(buf));
                    pts1bytes += readbytes;
                    if (0 == hup[0]) {
                        write(fdm[0], buf, readbytes);
                        pts12bytes += readbytes;
                    }
                }
            }
        }
    }
    free(buf);

    return EXIT_FAILURE;
}
